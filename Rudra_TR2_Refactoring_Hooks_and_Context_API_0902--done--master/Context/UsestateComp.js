import React,{useState, createContext} from 'react';
import Child from './Child'
import ChildA from './ChildA';
import ToggleComponent from './ToggleComponent';

export const AppContext = createContext(null);

const UsestateComp = () => {
    const [username, setUsername] = useState("");
    return (
        <AppContext.Provider value={{username, setUsername}}>
            <ChildA></ChildA>
            <Child></Child>
            <ToggleComponent></ToggleComponent>
        </AppContext.Provider>
    );
};

export default UsestateComp;