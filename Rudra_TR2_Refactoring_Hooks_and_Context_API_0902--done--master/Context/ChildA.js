import React, {useContext} from 'react';
import { AppContext } from './UsestateComp';
const ChildA = () => {
    const{username}=useContext(AppContext)
    return (
        <div>
            <h1>Username:{username}</h1>
        </div>
    );
};

export default ChildA;