import React, { useContext } from 'react';
import { AppContext } from './UsestateComp';

const Child = () => {
    const{setUsername}=useContext(AppContext)
    
    return (
        <div>
            <input onChange={(e) =>{
                setUsername(e.target.value)
            }}></input>
        </div>
    );
};

export default Child;