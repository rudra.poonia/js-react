import React from 'react';
import useToggle from './useToggle';

const ToggleComponent = () => {
    const [value, setValue]= useToggle(false)
    return (
        <div>
            {value.toString()}
            <button onClick={setValue}>toggle</button>
        </div>
    );
};

export default ToggleComponent;