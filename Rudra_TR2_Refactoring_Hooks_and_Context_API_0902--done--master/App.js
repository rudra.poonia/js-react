import './App.css';
import UsestateComp from './Context/UsestateComp';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <UsestateComp></UsestateComp>
      </header>
    </div>
  );
}

export default App;