import './App.css';
import Test from './Test';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Test></Test>
      </header>
    </div>
  );
}

export default App;